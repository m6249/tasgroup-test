import { DefaultTheme, NavigationContainer } from '@react-navigation/native'
import { createNativeStackNavigator } from '@react-navigation/native-stack'
import * as React from 'react'
import Toast from 'react-native-toast-message'
import { Provider } from 'react-redux'
import EventDetail from '~/screens/EventDetailScreen/EventDetail'
import EventsScreen from '~/screens/EventsScreen'
import store from '~/store'
import { SCREEN_NAME } from '~/utils/screens'

const Stack = createNativeStackNavigator()

const appTheme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    background: 'white',
  },
}

function App() {
  return (
    <Provider store={store}>
      <NavigationContainer theme={appTheme}>
        <Stack.Navigator initialRouteName={SCREEN_NAME.EVENTS_SCREEN}>
          <Stack.Screen name={SCREEN_NAME.EVENTS_SCREEN} component={EventsScreen} />
          <Stack.Screen name={SCREEN_NAME.EVENT_DETAIL_SCREEN} component={EventDetail} />
        </Stack.Navigator>
      </NavigationContainer>
      <Toast />
    </Provider>
  )
}

export default App
