import React from 'react'
import { ActivityIndicator, StyleSheet, View } from 'react-native'

const Loader = () => {
  return (
    <View style={styles.container}>
      <ActivityIndicator animating />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
  },
})

export default Loader
