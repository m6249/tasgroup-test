import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

const EventDetail = ({ route }) => (
  <View style={styles.container}>
    <Text style={styles.title}>{route.params.event?.type}</Text>
  </View>
)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#F1F5F9',
  },
  title: {
    color: '#28224B',
    fontSize: 18,
    fontWeight: '600',
  },
})

export default EventDetail
