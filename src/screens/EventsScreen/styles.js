import { StyleSheet } from 'react-native'

export const styles = StyleSheet.create({
  itemWrapper: {
    width: '100%',
    padding: 12,
    borderRadius: 8,
    shadowOpacity: 0.5,
    shadowColor: 'rgba(194, 205, 232, 0.5)',
    backgroundColor: '#F1F5F9',
    margin: 20,
  },
  itemText: {
    color: '#28224B',
    fontSize: 18,
    fontWeight: '600',
  },
})
