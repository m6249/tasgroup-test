import { useNavigation } from '@react-navigation/native'
import React, { useCallback } from 'react'
import { Text, TouchableOpacity } from 'react-native'

import { SCREEN_NAME } from '../../../utils/screens'
import { styles } from '../styles'

const Card = ({ item }) => {
  const { navigate } = useNavigation()
  const handleCheckDetail = useCallback(() => navigate(SCREEN_NAME.EVENT_DETAIL_SCREEN, { event: item }), [item])

  return (
    <TouchableOpacity onPress={handleCheckDetail} activeOpacity={0.7} style={styles.itemWrapper}>
      <Text style={styles.itemText}>{item?.type}</Text>
    </TouchableOpacity>
  )
}

export default Card
