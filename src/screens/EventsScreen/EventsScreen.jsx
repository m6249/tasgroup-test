import { useIsFocused } from '@react-navigation/native'
import React, { useCallback, useEffect, useState } from 'react'
import { FlatList, Text } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'
import Loader from '~/components/Loader'
import { loadEvents } from '~/features/gitEvents/actions'

import EventItem from './EventItem'

export const MIN_REFRESH_INTERVAL = 15000

const EventList = () => {
  const dispatch = useDispatch()
  const isFocused = useIsFocused()
  const { events, isLoading, lastUpdateTime } = useSelector(state => state.gitEvents)

  const [isScrolling, setIsScrolling] = useState(false)

  const handleScrollBeginDrag = () => setIsScrolling(true)
  const handleScrollEndDrag = () => setIsScrolling(false)

  const handleRefresh = useCallback(() => {
    //Пользователь может обновить список на экране №1, если с времени последнего обновления прошло более 15 секунд.
    if (!lastUpdateTime || new Date() - lastUpdateTime > MIN_REFRESH_INTERVAL) {
      dispatch(loadEvents())
    }
  }, [loadEvents, lastUpdateTime])

  const renderItem = useCallback(({ item }) => <EventItem item={item} />, [])

  useEffect(() => {
    if (isFocused) {
      // Когда возвращаемся на экран №1 с экрана №2 список обновляется немедленно. Отсчет времени до возможности ручного обновления списка также начинается заново.
      dispatch(loadEvents(true))
    }
    const intervalId = setInterval(() => {
      if (!isScrolling) {
        //При прокрутке списка - обновление приостанавливается.
        dispatch(loadEvents())
      }
    }, 60000)

    return () => clearInterval(intervalId) //обновляем каждые 60 секунд
  }, [isFocused])

  return (
    <FlatList
      data={events}
      renderItem={renderItem}
      keyExtractor={item => item.id}
      refreshing={false}
      onRefresh={handleRefresh}
      onScrollBeginDrag={handleScrollBeginDrag}
      onScrollEndDrag={handleScrollEndDrag}
      ListEmptyComponent={() => {
        if (isLoading) {
          return <Loader />
        }
        return <Text>Нет данных</Text>
      }}
    />
  )
}

export default EventList
