import axios from 'axios'
import Toast from 'react-native-toast-message'

const request = async (cmd, method, data) => {
  try {
    const response = await axios.request({
      url: `/${cmd}`,
      timeout: 10000,
      baseURL: 'https://api.github.com/',
      method,
      [method === 'post' ? 'data' : 'params']: data,
    })

    return response.data
  } catch (e) {
    Toast.show({
      type: 'error',
      position: 'top',
      text1: 'Ошибка',
      text2: e.message,
    })
    const err = new Error()
    throw err
  }
}

export default {
  get: (cmd, data) => request(cmd, 'get', data),
  delete: (cmd, data) => request(cmd, 'delete', data),
  post: (cmd, data) => request(cmd, 'post', data),
}
