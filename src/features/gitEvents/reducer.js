import { ActionTypes } from './actions'

const initialState = {
  events: [],
  lastUpdateTime: 0,
  isLoading: false,
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case ActionTypes.UPDATE_EVENTS:
      return { ...state, events: action.payload.data, isLoading: false, lastUpdateTime: action.payload.timestamp }
    case ActionTypes.SET_LOADER:
      return { ...state, isLoading: action.payload }
    default:
      return state
  }
}

export default reducer
