import { MIN_REFRESH_INTERVAL } from '~/screens/EventsScreen/EventsScreen'
import requester from '~/utils/requester'

export const ActionTypes = {
  UPDATE_EVENTS: 'UPDATE_EVENTS',
  SET_LOADER: 'SET_LOADER',
}

export const loadEvents = isFirstRender => async (dispatch, getState) => {
  const { gitEvents } = getState()

  try {
    if (Date.now() - gitEvents.lastUpdateTime < MIN_REFRESH_INTERVAL) {
      dispatch({
        type: ActionTypes.SET_LOADER,
        payload: true,
      })
    }

    const data = await requester.get('events', { page: 1, per_page: 26 }) // запрашиваем 25 элементов

    dispatch({
      type: ActionTypes.UPDATE_EVENTS,
      payload: { data, timestamp: isFirstRender ? 0 : Date.now() },
    })
  } catch (e) {
    dispatch({
      type: ActionTypes.SET_LOADER,
      payload: false,
    })
  }
}
