import { applyMiddleware, combineReducers, createStore } from 'redux'
import thunk from 'redux-thunk'

import gitEventsReducer from '../features/gitEvents/reducer'

const reducer = combineReducers({
  gitEvents: gitEventsReducer,
})
const store = createStore(reducer, applyMiddleware(thunk))

export default store
